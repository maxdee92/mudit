def call() {

 post {
  aborted {
    slackSend channel: '#my_channel', message: 'build is aborted'
  }
  success {
    slackSend channel: '#my_channel', message: 'build is successful'
  }
  failure {
    slackSend channel: '#my_channel', message: 'build is failed'
  }
 }
}
